import React, { useState, useEffect, FormEvent } from "react";
import { GoogleMap, InfoWindow } from "@react-google-maps/api";

import { Container, Box } from "./styles";

interface Location {
  lat: number;
  lng: number;
}

interface Points {
  position: {
    lat: number;
    lng: number;
  };
  title: string;
}

const Home: React.FC = () => {
  const [position, setPosition] = useState<Location>({
    lat: 0,
    lng: 0,
  });

  const [latitude, setLatitude] = useState(0);
  const [longitude, setLongitude] = useState(0);

  const [title, setTitle] = useState("");

  const [points, setPoints] = useState<Points[]>([]);

  useEffect(() => {
    navigator.geolocation.getCurrentPosition((position) => {
      const { latitude, longitude } = position.coords;

      setPosition({
        lat: latitude,
        lng: longitude,
      });
    });
  }, []);

  function handleMapClick(event: FormEvent) {
    setPoints([
      ...points,
      {
        position: {
          lat: latitude,
          lng: longitude,
        },
        title,
      },
    ]);
  }

  return (
    <Container>
      <Box>
        <div className="inputs">
          <p>
            Coloque coordenadas geográficas e um título para criar um ponto no
            mapa
          </p>
          <div className="lat-lng">
            <input
              placeholder="Latitude"
              onChange={(event) => setLatitude(Number(event.target.value))}
            />
            <input
              placeholder="Longitude"
              onChange={(event) => setLongitude(Number(event.target.value))}
            />
          </div>
          <div className="text">
            <input
              placeholder="Título"
              onChange={(event) => setTitle(event.target.value)}
            />
            <button onClick={(event) => handleMapClick(event)}>
              Adicionar
            </button>
          </div>
        </div>
        <div className="maps">
          <GoogleMap
            mapContainerStyle={{
              height: "100%",
              width: "100%",
              borderBottomLeftRadius: "16px",
              borderBottomRightRadius: "16px",
            }}
            center={position}
            zoom={13}
          >
            {points.length > 0 &&
              points.map((point) => (
                <InfoWindow position={point.position}>
                  <h2>{point.title}</h2>
                </InfoWindow>
              ))}
          </GoogleMap>
        </div>
      </Box>
    </Container>
  );
};

export default Home;
