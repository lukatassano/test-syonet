import styled from "styled-components";

export const Container = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  width: 100vw;
  border-bottom: 1px solid #d0d0d1;

  img {
    margin: 30px 0;
  }

  @media (max-width: 600px) {
    img {
      margin: 16px 0;
    }
  }
`;
