import React from "react";

import Logo from "../../assets/logo-syonet.svg";
import { Container } from "./styles";

const Header: React.FC = () => {
  return (
    <Container>
      <img src={Logo} alt="Logo" />
    </Container>
  );
};

export default Header;
