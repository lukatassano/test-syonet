import styled from "styled-components";

export const Container = styled.div`
  display: flex;
  width: 100vw;
  height: 85vh;
  align-items: center;
  justify-content: center;
`;

export const Box = styled.div`
  box-shadow: 0px 10px 30px rgba(0, 0, 0, 0.3);
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: space-around;
  border-radius: 16px;
  height: 70vh;
  width: 50vw;
  background: #eee;

  .inputs {
    display: flex;
    flex: 1;
    width: 90%;
    flex-direction: column;
    align-items: center;
    justify-content: center;

    p {
      font-size: 18px;
      font-weight: 200;
      color: #777;
      margin: 8px 8px 18px 18px;
      align-self: baseline;
    }

    .lat-lng {
      display: flex;
      width: 100%;

      input {
        padding: 0 15px 0 15px;
        border-radius: 24px;
        border: 0;
        width: 100%;
        height: 48px;
        margin: 10px;
        font-size: 16px;
      }
    }

    .text {
      display: flex;
      width: 100%;
      align-items: center;

      input {
        padding: 0 15px 0 15px;
        border-radius: 24px;
        border: 0;
        width: 100%;
        height: 48px;
        margin: 10px;
        font-size: 16px;
      }

      button {
        height: 48px;
        width: 150px;
        border: 0;
        color: #fff;
        background: #ff4055;
        border-radius: 24px;
        font-weight: bold;
        margin-right: 10px;
        font-size: 16px;
        cursor: pointer;
        transition: 0.3s;

        &:hover {
          background: #d00000;
        }
      }
    }
  }

  .maps {
    display: flex;
    flex: 1;
    align-items: center;
    justify-content: center;
    width: 100%;
  }

  @media (max-width: 900px) {
    width: 90vw;
    height: 75vh;

    .inputs {
      p {
        font-size: 16px;
      }

      .lat-lng {
        input {
          border-radius: 18px;
          height: 36px;
          margin: 5px;
          font-size: 14px;
        }
      }

      .text {
        display: flex;
        width: 100%;
        align-items: center;

        input {
          border-radius: 18px;
          border: 0;
          width: 100%;
          height: 36px;
          margin: 5px;
          font-size: 14px;
        }

        button {
          height: 36px;
          width: 150px;
          border-radius: 18px;
          margin-right: 5px;
          font-size: 14px;
        }
      }
    }
  }
`;
