import React from "react";

import Header from "./components/Header";
import Home from "./pages/Home";

import "./global.css";

const src: React.FC = () => {
  return (
    <>
      <Header />
      <Home />
    </>
  );
};

export default src;
